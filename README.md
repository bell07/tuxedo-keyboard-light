# keyboard-light

## Description
Simple python script to set the keyboard backlight and color to the CPU-Load value.
I run the script as root since the files `/sys/devices/platform/tuxedo_keyboard/leds/rgb:kbd_backlight/*` are writteable by root only.


## Installation
Check the device does existing in your linux, or have maybe different name.
Clone the repo or download the "run.py" file. Adjust the file name in run.py

## License
GPL 3.0 and above
