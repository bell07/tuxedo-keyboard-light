#!/usr/bin/env python

from time import sleep

LED_BRIGHTNESS_FILE = "/sys/devices/platform/tuxedo_keyboard/leds/rgb:kbd_backlight/brightness"
LED_COLOR_FILE = "/sys/devices/platform/tuxedo_keyboard/leds/rgb:kbd_backlight/multi_intensity"

LED_BRIGHTNESS = 255
START_WITH_PERCENT = 0.05  # up t0 5% load is "idle"

cpu_active_prev = None
cpu_total_prev = None
brightness_prev = None
brightness_delta = None

def debug(message):
    #    print(message)
    pass


def get_current_stats():
    f = open("/proc/stat")

    while True:
        line = f.readline()
        if not line:
            f.close()
            break
        values = line.split()
        if values[0] == "cpu":
            user = int(values[1])
            nice = int(values[2])
            system = int(values[3])
            idle = int(values[4])
            iowait = int(values[5])
            irq = int(values[6])
            softirq = int(values[7])
            steal = int(values[8])
            guest = int(values[9])

            cpu_active = user + system + nice + softirq + steal
            cpu_total = user + system + nice + softirq + steal + idle + iowait

            f.close()
            return cpu_active, cpu_total


def get_cpuload(cpu_active_cur, cpu_total_cur):
    return (cpu_active_cur - cpu_active_prev) / (cpu_total_cur - cpu_total_prev)


def set_color(cpu_load):
    stretched = cpu_load * 2 - 1
    if stretched < 0:
        r = 0
        g = int((1 + stretched) * 255)
        b = int(-stretched * 255)

    else:
        r = int(stretched * 255)
        g = int((1 - stretched) * 255)
        b = 0

    parsed_string = f'{r} {g} {b}'
    debug(f'Set color values to {parsed_string}')
    f = open(LED_COLOR_FILE, "w")
    f.write(parsed_string)
    f.close()


def set_brightness(cpu_load):
    # Get maybe changed current brightness
    f = open(LED_BRIGHTNESS_FILE, "r")
    brightness_curr = int(f.read())
    f.close()

    # Check if brightness was changed in other way. Compare to stored brightness_prev
    global brightness_prev
    global brightness_delta
    if brightness_prev is None:
        brightness_delta = 0
    else:
        brightness_delta = brightness_delta + brightness_curr - brightness_prev
    debug(f'brightness delta is {brightness_delta}')

    # Calculate new value
    if cpu_load < START_WITH_PERCENT:
        brightness = 0
    else:
        brightness = round(LED_BRIGHTNESS * (cpu_load - START_WITH_PERCENT) / (1 - START_WITH_PERCENT))

    # Apply delta and adjust
    brightness = brightness + brightness_delta
    if brightness < 0:
        brightness = 0
    elif brightness > LED_BRIGHTNESS:
        brightness = LED_BRIGHTNESS

    # Apply new value
    debug(f'Set brightness value to {brightness}')
    f = open(LED_BRIGHTNESS_FILE, "w")
    f.write(str(brightness))
    f.close()
    brightness_prev = brightness


def do_loop():
    global cpu_active_prev
    global cpu_total_prev
    cpu_active_cur, cpu_total_cur = get_current_stats()

    if cpu_active_prev is not None:
        cpu_load = get_cpuload(cpu_active_cur, cpu_total_cur)
        debug(f'Current CPU load is {format(cpu_load, ".2%")}')
        set_brightness(cpu_load)
        set_color(cpu_load)

    cpu_active_prev, cpu_total_prev = cpu_active_cur, cpu_total_cur
    sleep(1)


while True:
    do_loop()
